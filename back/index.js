const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const bodyParser = require('body-parser');
const cors = require('cors');

const mongoDB = 'mongodb://127.0.0.1:27017/passport-jwt';
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
// mongoose.connect('mongodb+srv://Svitlana:sinergy!23@cluster0.jjrsb.mongodb.net/crazylabs?retryWrites=true&w=majority', { useUnifiedTopology: true });
mongoose.connection.on('error', error => console.log(error));
mongoose.Promise = global.Promise;

require('./auth/auth');

const routes = require('./routes/routes');
const secureRoute = require('./routes/secure-routes');

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/', routes);

// Plug in the JWT strategy as a middleware so only verified users can access this route.
app.use('/api', passport.authenticate('jwt', { session: false }), secureRoute);

// app.get('/api/data', (req, res) =>{
//   console.log(req)
//   const response = {
//     success: true,
//   };
//
//   return res.status(200).json(response);
// });

// Handle errors.
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({ error: err });
});

app.listen(3000, () => {
  console.log('Server started.');
});
