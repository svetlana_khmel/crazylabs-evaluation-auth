const express = require('express');
const router = express.Router();
const Entry = require('../models/entry.js');
const UserModel = require('../models/model');
const fs = require('fs');

router.get('/', (request, response, next) => {
  response.send(request.user);
});

router.get('/profile', (req, res, next) => {
  res.json({
    message: 'You made it to the secure route',
    user: req.user,
    token: req.query.secret_token
  });
});

const sendData = (data, res) => {
  const json = JSON.parse(data);

  res.header('Content-Type', 'application/json');
  res.send(json);
}

router.get('/data', (req, res, next) => {
  // Entry.find({}, 'email', function(err, data){
  //   if(err) return next(err);
  //
  //   res.status(200).send(data);
  // });
  const user = req.user.email;
  fs.readFile(`./data/${user}.json`, (err, data) => {
    if (err) {
      fs.readFile('./data/data', (err, data1) => {
        if (err) throw err;
        sendData(data, res);
      })
    }
    sendData(data, res);
  });
});

const saveData = (data, user, callback) => {
  fs.writeFile(`./data/${user}.json`, JSON.stringify(data), callback);
};

router.post('/save', (req, res, next) => {
  console.log('save ', req);

  // const entry = new Entry({
  //   data: req.body
  // });
  //
  // entry.save((err, data) => {
  //   if (err) throw err;
  //   res.status(200).send('Saved.')
  // })

  const data = req.body;
  console.log('req.body', req.body);

  saveData(data, req.user.email, err => {
    if (err) {
      res.status(404).send('Not saved.');
      return;
    }
    res.status(200).send('Updated');
  });
});

module.exports = router;
