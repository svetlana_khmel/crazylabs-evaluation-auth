const express = require('express');
const passport = require('passport');

const router = express.Router();
const jwt = require('jsonwebtoken');
const Entry = require('../models/entry.js');

router.post('/signup', passport.authenticate('signup', { session: false }), async (req, res, next) => {
  res.json({
    message: 'Signup successful',
    user: req.user
  });
});

router.post('/login', async (req, res, next) => {
  passport.authenticate('login', async (err, user, info) => {
    console.log('err', err);
    console.log('user', user);
    console.log('info---', info);

    try {
      if (err || !user) {
        console.log('0');
        const error = new Error('An error occurred.');
        console.log('err 1', err);
        return next(error);
      }

      req.login(user, { session: false }, async error => {
        if (error) return next(error);
        console.log(' 2');
        const body = { _id: user._id, email: user.email };
        const token = jwt.sign({ user: body }, 'TOP_SECRET');

        return res.json({ token });
      });
    } catch (error) {
      console.log('error');
      return next(error);
    }
  })(req, res, next);
});

module.exports = router;
