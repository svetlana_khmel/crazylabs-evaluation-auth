const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

// const userSchema = new Schema({
//   name: {
//     type: String,
//     required: true},
//   count: {
//     type: Number,
//     required: true},
//   owner:  {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: 'User'
//   }
// });

const EntrySchema = new Schema({
  data: [{}],
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

module.exports = mongoose.model('Entry', EntrySchema);
