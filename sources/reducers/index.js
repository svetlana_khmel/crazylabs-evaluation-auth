import { v4 as uuidv4 } from 'uuid';
import * as actionTypes from '../actions/actionTypes';

const initialState = {
  rows: {
    past: [],
    present: [
      { name: 'Foo', count: 5, uid: uuidv4() },
      { name: 'Bar', count: 12, uid: uuidv4() },
      { name: 'Lorem', count: 4, uid: uuidv4() },
      { name: 'Ispum', count: 7, uid: uuidv4() }
    ],
    future: []
  },
  recoverData: [],
  updated: []
};

const reducer = (state = initialState, action) => {
  const { past } = state.rows;
  const { future } = state.rows;

  switch (action.type) {
    case actionTypes.SET_AUTH:
      return { ...state, isAuthenticated: true };
    case actionTypes.LOGOUT:
      return { ...state, isAuthenticated: false };
    case actionTypes.SET_DATA:
      return { ...state, rows: { ...state.rows, present: action.payload }, recoverData: action.payload };

    case actionTypes.SET_UPDATED:
      const updated = action.payload;
      return { ...state, updated };

    case actionTypes.RESET:
      return {
        ...state,
        rows: {
          ...state.rows,
          present: [
            { name: '', count: '', uid: uuidv4() },
            { name: '', count: '', uid: uuidv4() },
            { name: '', count: '', uid: uuidv4() },
            { name: '', count: '', uid: uuidv4() }
          ]
        }
      };
    case actionTypes.CANCEL:
      return { ...state, rows: { ...state.rows, present: state.recoverData } };

    case actionTypes.SAVE:
      return { ...state, tags: action.payload };

    case actionTypes.UNDO:
      const newPast = [...past];
      let newFuture = [...future] || [];
      newFuture = [state.rows.present, ...newFuture];
      newPast.splice(-1, 1);
      const presentUndo = past[past.length - 1];

      if (past.length === 0) return { ...state };

      return {
        ...state,
        rows: {
          ...state.rows,
          past: newPast,
          present: presentUndo,
          future: newFuture
        }
      };

    case actionTypes.REDO:
      const newPastRedo = [...past, state.rows.present];
      let newFutureRedo = [...future] || [];
      const presentRedo = future[0];
      newFutureRedo.length > 1 ? newFutureRedo.shift() : (newFutureRedo = []);

      if (future.length === 0) return { ...state };

      return {
        ...state,
        rows: {
          ...state.rows,
          past: newPastRedo,
          present: presentRedo,
          future: newFutureRedo
        }
      };

    case actionTypes.PUT_HISTORY:
      return { ...state, rows: { ...state.rows, past: [...past, state.rows.present], present: action.payload } };

    case actionTypes.ADD_ROW:
      const present = [...state.rows.present];
      present.splice(action.payload + 1, 0, { name: '', count: '', uid: uuidv4() });
      return { ...state, rows: { ...state.rows, present } };

    case actionTypes.DELETE_ROW:
      return {
        ...state,
        rows: { ...state.rows, present: state.rows.present.filter((el, index) => index !== action.payload) }
      };

    default:
      return state;
  }
};

export default reducer;
