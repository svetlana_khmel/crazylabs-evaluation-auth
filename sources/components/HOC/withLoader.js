import React, { Suspense } from 'react';
import Loader from '../loader';

function withLoader(Component) {
  return function WithLoaderComponent({ isLoading, ...props }) {
    if (!isLoading) {
      return (
        <Suspense fallback={<div> Loading...</div>}>
          <Component {...props} />
        </Suspense>
      );
    }
    return <Loader />;
  };
}

export default withLoader;
