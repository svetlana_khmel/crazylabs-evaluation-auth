import React, { Suspense } from 'react';
import Loader from '../loader';

function withSuspense(Component) {
  return function({ ...props }) {
    return (
      <Suspense fallback={<div> Loading...</div>}>
        <Component {...props} />
      </Suspense>
    );
  };
}

export default withSuspense;
