import React from 'react';
import { useSelector } from 'react-redux';
import { Line } from 'react-chartjs-2';

const LineChartData = (labels, data) => ({
  labels,
  datasets: [
    {
      label: 'line',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(75,192,192,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data
    }
  ]
});

export default () => {
  const data = useSelector(state => state.rows.present);

  const labels = data.reduce((acc, current) => [...acc, current.name], []);
  const count = data.reduce((acc, current) => [...acc, current.count], []);
  const chartData = LineChartData(labels, count);

  return (
    <div>
      <Line data={chartData} />
    </div>
  );
};
