import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { doLogin, doRegister } from '../actions';
import Button from './button';

const Login = ({ type }) => {
  const dispatch = useDispatch();
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = e => {
    e.preventDefault();
    if (type === 'login') {
      dispatch(doLogin({ email: login, password: password }));
    } else {
      dispatch(doRegister({ username: login, password: password }));
    }
  };

  const handleLogin = e => {
    setLogin(e.target.value);
  };

  const handlePassword = e => {
    setPassword(e.target.value);
  };

  return (
    <div className="login-container">
      <form>
        {type === 'login' ? <h1>Login form</h1> : <h1>Register form</h1>}
        <fieldset>
          <label>
            <p>{type === 'login' ? 'Login' : 'Register'}</p>
            <input type="text" onChange={handleLogin} name="email" />
          </label>
        </fieldset>
        <fieldset>
          <label>
            <p>Password</p>
            <input type="password" onChange={handlePassword} name="name" />
          </label>
        </fieldset>
        <Button type="submit" value="Submit" event={handleSubmit} name="submit">
          Submit
        </Button>
      </form>
    </div>
  );
};

export default Login;
