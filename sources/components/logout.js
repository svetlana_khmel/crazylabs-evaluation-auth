import React, { memo } from 'react';
import { useDispatch } from 'react-redux';
import { doLogout } from '../actions';

const Logout = () => {
  const dispatch = useDispatch();
  const logout = () => {
    localStorage.removeItem('user');
    dispatch(doLogout());
  };

  return (
    <a href="/" onClick={logout}>
      logout
    </a>
  );
};

export default memo(Logout);
