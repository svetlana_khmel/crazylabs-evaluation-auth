import React, { useCallback, useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { addRowAction, deleteRowAction, setHistory } from '../actions';
import Button from './button';

const Row = ({ id, index, name, count, updateRows }) => {
  const dispatch = useDispatch();
  const [values, setValues] = useState({ name, count });

  const handleChange = (event, name) => {
    setValues({ ...values, [name]: event.target.value });
  };

  const addRow = useCallback(() => {
    dispatch(setHistory());
    dispatch(addRowAction(index));
  });
  const deleteRow = useCallback(() => {
    dispatch(setHistory());
    dispatch(deleteRowAction(index));
  });

  updateRows(id, index, { ...values });
  return (
    <div className="row">
      <input type="text" readOnly value={index} />
      <input type="text" onChange={e => handleChange(e, 'name')} value={values.name} />
      <input type="text" onChange={e => handleChange(e, 'count')} value={values.count} />
      <Button value="+" name="add" event={addRow} />
      <Button value="-" name="remove" event={deleteRow} />
    </div>
  );
};

export default Row;
