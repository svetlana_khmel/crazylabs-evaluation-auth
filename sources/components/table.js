import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setUpdated } from '../actions';
import Row from './row';

const Table = () => {
  const rows = useSelector(state => state.rows.present);
  const dispatch = useDispatch();
  const updated = [...rows];

  const updateRows = (uid, index, data) => {
    updated[index] = { ...data, uid };
    dispatch(setUpdated(updated));
  };

  const renderRows = () =>
    rows.map((row, index) => (
      <Row key={row.uid} id={row.uid} index={index} name={row.name} count={row.count} updateRows={updateRows} />
    ));

  return <>{renderRows()}</>;
};

export default Table;
