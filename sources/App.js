import React, { useEffect, lazy, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { resetData, cancelData, saveData, getData, setHistory, undoState, redoState } from './actions';
import withLoader from './components/HOC/withLoader';
import withSuspense from './components/HOC/withSuspense';

import './styles.scss';

const Table = lazy(() => import('./components/table'));
const Button = lazy(() => import('./components/button'));
const Login = lazy(() => import('./components/login'));
const Line = lazy(() => import('./components/charts/line'));
const Bar = lazy(() => import('./components/charts/bar'));
const Logout = lazy(() => import('./components/Logout'));

const TableWithLoader = withLoader(Table);
const ButtonWithSuspense = withSuspense(Button);
const LoginWithSuspense = withSuspense(Login);
const LineWithSuspense = withSuspense(Line);
const BarWithSuspense = withSuspense(Bar);
const LogoutwithSuspense = withSuspense(Logout);

const App = () => {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector(state => state.isAuthenticated);

  useEffect(() => {
    if (isAuthenticated) {
      dispatch(getData());
    }
  }, [isAuthenticated]);

  const undo = useCallback(() => dispatch(undoState()));
  const redo = useCallback(() => dispatch(redoState()));

  const save = useCallback(() => {
    dispatch(setHistory());
    dispatch(saveData());
  });
  const reset = useCallback(() => {
    dispatch(setHistory());
    dispatch(resetData());
  });
  const cancel = useCallback(() => {
    dispatch(setHistory());
    dispatch(cancelData());
  });
  if (!isAuthenticated) {
    return (
      <>
        <LoginWithSuspense type="login" />
        <LoginWithSuspense type="register" />
      </>
    );
  }
  return (
    <>
      <div className="header">
        <LogoutwithSuspense />
        <ButtonWithSuspense value="Undo" name="undo" event={undo} />
        <ButtonWithSuspense value="Redo" name="redo" event={redo} />
      </div>
      <TableWithLoader />
      <ButtonWithSuspense value="Save" name="save" event={save} />
      <ButtonWithSuspense value="Reset" name="reset" event={reset} />
      <ButtonWithSuspense value="Cancel" name="cancel" event={cancel} />
      <div className="line-chart">
        <LineWithSuspense />
      </div>
      <div className="bar-chart">
        <BarWithSuspense />
      </div>
    </>
  );
};

export default App;
