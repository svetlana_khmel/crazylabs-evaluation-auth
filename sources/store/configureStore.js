import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';

const middlewares = applyMiddleware(thunk);

export default function() {
  return createStore(rootReducer, middlewares);
}
