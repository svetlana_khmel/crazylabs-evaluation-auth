import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import * as actionTypes from './actionTypes';
import md5 from 'js-md5';
import querystring from 'querystring';
import Auth from '../auth';

const url = 'http://localhost:3000';
const token = Auth();

const addRowAction = data => ({ type: actionTypes.ADD_ROW, payload: data });
const deleteRowAction = data => ({ type: actionTypes.DELETE_ROW, payload: data });
const resetData = () => ({ type: actionTypes.RESET });
const cancelData = () => ({ type: actionTypes.CANCEL });
const setData = data => ({ type: actionTypes.SET_DATA, payload: data });
const putHistory = data => ({ type: actionTypes.PUT_HISTORY, payload: data });
const undoState = () => ({ type: actionTypes.UNDO });
const redoState = () => ({ type: actionTypes.REDO });
const setUpdated = data => ({ type: actionTypes.SET_UPDATED, payload: data });
const setAuthData = () => ({ type: actionTypes.SET_AUTH });
const doLogout = () => ({ type: actionTypes.LOGOUT });

const getData = () => dispatch => {
  if (token) {
    axios
      .get(`${url}/api/data`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(resp => {
        console.log('getData', resp);
        const dataWithID = resp.data.map(el => {
          el.uid = uuidv4();
          return el;
        });

        dispatch(setData(dataWithID));
      })
      .catch(error => {
        console.log(error);
      });
  }
};

// const saveData = data => (dispatch, getState) => {
//   const currentState = getState();
//   const data = currentState.updated;
//
//   axios
//     .post(`${url}/api/save`, data, { headers: { 'content-type': 'application/json' } })
//     .then(data => {
//       console.log(data);
//     })
//     .catch(error => console.log(error));
// };

const saveData = () => (dispatch, getState) => {
  const currentState = getState();
  const data = currentState.updated;

  axios
    .post(`${url}/api/save`, data, {
      headers: {
        Authorization: `Basic ${token}`
      }
    })
    .catch(error => console.log(error));

  if (token) {
    axios
      .post(`${url}/api/save`, data, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(resp => {
        console.log('saved', resp);
      })
      .catch(error => {
        console.log(error);
      });
  }
};

const setHistory = () => (dispatch, getState) => {
  const currentState = getState();
  dispatch(putHistory(currentState.rows.present));
};

const doLogin = data => (dispatch, getState, arg) => {
  console.log('data', data);
  console.log('data', arg);
  axios
    .post(
      `${url}/login`,
      querystring.stringify({
        email: data.email,
        password: data.password
      }),
      {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
      }
    )
    .then(data => {
      console.log('data', data);
      if (data.data.token) {
        localStorage.setItem('user', JSON.stringify(data.data));
      }
      dispatch(setAuthData(data));
    })

    .catch(error => {
      console.log(error);
    });
};

const doRegister = data => dispatch => {
  axios
    .post(
      `${url}/signup`,
      querystring.stringify({
        username: data.username,
        password: data.password
      }),
      {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
      }
    )
    .then(data => {
      console.log(data);
      dispatch(setAuthData());
    })
    .catch(error => {
      console.log(error);
    });
};

export {
  getData,
  addRowAction,
  deleteRowAction,
  resetData,
  cancelData,
  saveData,
  setHistory,
  undoState,
  redoState,
  doLogin,
  doRegister,
  doLogout,
  setUpdated
};
